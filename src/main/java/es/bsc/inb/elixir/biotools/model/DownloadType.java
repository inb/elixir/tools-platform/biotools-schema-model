/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */
package es.bsc.inb.elixir.biotools.model;

/**
 * Type of download that is linked to.
 * 
 * @author Dmitry Repchevsky
 */

public enum DownloadType {
    API_SPECIFICATION("API specification"),
    BIOLOGICAL_DATA("Biological data"),
    BINARIES("Binaries"),
    COMMANDLINE_SPECIFICATION("Command-line specification"),
    CONTAINER_FILE("Container file"),
    DOWNLOAD_PAGE("Downloads page"),
    ICON("Icon"),
    SOFTWARE_PACKEGE("Software package"),
    SCREENSHOT("Screenshot"),
    SOURCE_CODE("Source code"),
    TEST_DATA("Test data"),
    TEST_SCRIPT("Test script"),
    CWL_TOOL_WRAPPER("Tool wrapper (CWL)"),
    GALAXY_TOOL_WRAPPER("Tool wrapper (Galaxy)"),
    TAVERNA_TOOL_WRAPPER("Tool wrapper (Taverna)"),
    OTHER_TOOL_WRAPPER("Tool wrapper (Other)"),
    VM_IMAGE("VM image"),
    OTHER("Other");
    
    public final String value;
    
    private DownloadType(final String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static DownloadType fromValue(final String value) {
        for (DownloadType type: DownloadType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(value);
    }
}
