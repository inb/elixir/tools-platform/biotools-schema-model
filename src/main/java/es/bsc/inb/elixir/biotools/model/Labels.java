/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeDeserializer;
import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeListDeserializer;
import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeListSerializer;
import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeSerializer;
import java.util.List;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

/**
 * @author Dmitry Repchevsky
 */

public class Labels {

    private List<ToolType> toolTypes;
    private List<Topic> topics;
    private List<OperatingSystemType> operatingSystems;
    private List<LanguageType> languages;
    private LicenseType license;
    private List<String> collectionIDs;
    private MaturityType maturity;
    private CostType cost;
    private List<AccessibilityType> accessibilities;
    private List<ElixirPlatform> elixirPlatforms;
    private List<ElixirNode> elixirNodes;

    @JsonbProperty("toolType")
    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<ToolType> getToolTypes() {
        return toolTypes;
    }

    @JsonbProperty("toolType")
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setToolTypes(final List<ToolType> toolTypes) {
        this.toolTypes = toolTypes;
    }
    
    @JsonbProperty("topic")
    public List<Topic> getTopics() {
        return topics;
    }

    @JsonbProperty("topic")
    public void setTopics(final List<Topic> topics) {
        this.topics = topics;
    }
    
    @JsonbProperty("operatingSystem")
    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<OperatingSystemType> getOperatingSystems() {
        return operatingSystems;
    }

    @JsonbProperty("operatingSystem")
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setOperatingSystems(final List<OperatingSystemType> operatingSystems) {
        this.operatingSystems = operatingSystems;
    }
    
    @JsonbProperty("language")
    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<LanguageType> getLanguages() {
        return languages;
    }
    
    @JsonbProperty("language")
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setLanguages(final List<LanguageType> languages) {
        this.languages = languages;
    }

    @JsonbTypeDeserializer(EnumTypeDeserializer.class)
    public LicenseType getLicense() {
        return license;
    }

    @JsonbTypeSerializer(EnumTypeSerializer.class)
    public void setLicense(final LicenseType license) {
        this.license = license;
    }

    @JsonbProperty("collectionID")
    public List<String> getCollectionIDs() {
        return collectionIDs;
    }

    @JsonbProperty("collectionID")
    public void setCollectionIDs(final List<String> collectionIDs) {
        this.collectionIDs = collectionIDs;
    }
    
    @JsonbTypeDeserializer(EnumTypeDeserializer.class)
    public MaturityType getMaturity() {
        return maturity;
    }
    
    @JsonbTypeSerializer(EnumTypeSerializer.class)
    public void setMaturity(final MaturityType maturity) {
        this.maturity = maturity;
    }
    
    @JsonbTypeDeserializer(EnumTypeDeserializer.class)
    public CostType getCost() {
        return cost;
    }
    
    @JsonbTypeSerializer(EnumTypeSerializer.class)
    public void setCost(final CostType cost) {
        this.cost = cost;
    }
    
    @JsonbProperty("accessibility")
    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<AccessibilityType> getAccessibilities() {
        return accessibilities;
    }

    @JsonbProperty("accessibility")
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setAccessibilities(final List<AccessibilityType> accessibilities) {
        this.accessibilities = accessibilities;
    }
    
    @JsonbProperty("elixirPlatform")
    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<ElixirPlatform> getElixirPlatforms() {
        return elixirPlatforms;
    }
    
    @JsonbProperty("elixirPlatform")
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setElixirPlatforms(final List<ElixirPlatform> elixirPlatforms) {
        this.elixirPlatforms = elixirPlatforms;
    }
    
    @JsonbProperty("elixirNode")
    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<ElixirNode> getElixirNodes() {
        return elixirNodes;
    }
    
    @JsonbProperty("elixirNode")
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setElixirNodes(final List<ElixirNode> elixirNodes) {
        this.elixirNodes = elixirNodes;
    }
}