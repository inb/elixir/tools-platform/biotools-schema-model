/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

/**
 * @author Dmitry Repchevsky
 */

public enum ToolLinkType {
    DISCUSSION_FORUM("Discussion forum"),
    GALAXY_SERVICE("Galaxy service"),
    HELPDESK("Helpdesk"),
    ISSUE_TRACKER("Issue tracker"),
    MAILING_LIST("Mailing list"),
    MIRROR("Mirror"),
    REGISTRY("Registry"),
    REPOSITORY("Repository"),
    SERVICE("Service"),
    SOCIAL_MEDIA("Social media"),
    SOFTWARE_CATALOGUE("Software catalogue"),
    SCIENTIFIC_BENCHMARK("Scientific benchmark"),
    TECHNICAL_MONITORING("Technical monitoring"),
    OTHER("Other");
    
    public final String value;
    
    private ToolLinkType(final String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static ToolLinkType fromValue(final String value) {
        for (ToolLinkType type: ToolLinkType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(value);
    }
}
