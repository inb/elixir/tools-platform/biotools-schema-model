/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

/**
 * @author Dmitry Repchevsky
 */

public enum LanguageType {
    ACTION_SCRIPT("ActionScript"),
    ADA("Ada"),
    APPLE_SCRIPT("AppleScript"),
    ASSEMBLY_LANGUAGE("Assembly language"),
    AWK("AWK"),
    BASH("Bash"),
    C("C"),
    CSharp("C#"),
    CPP("C++"),
    COBOL("COBOL"),
    COLD_FUSION("ColdFusion"),
    CWL("CWL"),
    D("D"),
    DELPHI("Delphi"),
    DYLAN("Dylan"),
    EIFFEL("Eiffel"),
    FORTH("Forth"),
    FORTRAN("Fortran"),
    GROOVY("Groovy"),
    HASKELL("Haskell"),
    ICARUS("Icarus"),
    JAVA("Java"),
    JAVASCRIPT("JavaScript"),
    JULIA("Julia"),
    JSP("JSP"),
    LABVIEW("LabVIEW"),
    LISP("Lisp"),
    LUA("Lua"),
    MAPLE("Maple"),
    MATHEMATICA("Mathematica"),
    MATLAB("MATLAB"),
    MLXTRAN("MLXTRAN"),
    NMTRAN("NMTRAN"),
    OCAML("OCaml"),
    PASCAL("Pascal"),
    PERL("Perl"),
    PHP("PHP"),
    PROLOG("Prolog"),
    PYMOL("PyMOL"),
    PYTHON("Python"),
    R("R"),
    RACKET("Racket"),
    REXX("REXX"),
    RUBY("Ruby"),
    SAS("SAS"),
    SCALA("Scala"),
    SCHEME("Scheme"),
    SHELL("Shell"),
    SMALLTALK("Smalltalk"),
    SQL("SQL"),
    TURING("Turing"),
    VERILOG("Verilog"),
    VHDL("VHDL"),
    VISUAL_BASIC("Visual Basic"),
    XAML("XAML"),
    OTHER("Other");
    
    public final String value;
    
    private LanguageType(final String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static LanguageType fromValue(final String value) {
        for (LanguageType type: LanguageType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(value);
    }
}
