/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

import java.util.List;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Tool {

    private Summary summary;
    private List<Function> functions;
    private Labels labels;
    private List<Link> links;
    private List<Download> downloads;
    private List<Documentation> documentations;
    private List<Relation> relations;
    private List<Publication> publications;
    private List<Credit> credits;
    
    public Summary getSummary() {
        return summary;
    }

    public void setSummary(final Summary summary) {
        this.summary = summary;
    }

    @JsonbProperty("function")
    public List<Function> getFunctions() {
        return functions;
    }

    @JsonbProperty("function")
    public void setFunctions(final List<Function> functions) {
        this.functions = functions;
    }

    public Labels getLabels() {
        return labels;
    }

    public void setLabels(final Labels labels) {
        this.labels = labels;
    }

    @JsonbProperty("link")
    public List<Link> getLinks() {
        return links;
    }

    @JsonbProperty("link")
    public void setLinks(final List<Link> links) {
        this.links = links;
    }
    
    @JsonbProperty("download")
    public List<Download> getDownloads() {
        return downloads;
    }

    @JsonbProperty("download")
    public void setDownloads(final List<Download> downloads) {
        this.downloads = downloads;
    }
    
    @JsonbProperty("documentation")
    public List<Documentation> getDocumentations() {
        return documentations;
    }

    @JsonbProperty("documentation")
    public void setDocumentations(final List<Documentation> documentations) {
        this.documentations = documentations;
    }
    
    @JsonbProperty("relation")
    public List<Relation> getRelations() {
        return relations;
    }
    
    @JsonbProperty("relation")
    public void setRelations(final List<Relation> relations) {
        this.relations = relations;
    }

    @JsonbProperty("publication")
    public List<Publication> getPublications() {
        return publications;
    }
    
    @JsonbProperty("publication")
    public void setPublications(final List<Publication> publications) {
        this.publications = publications;
    }

    @JsonbProperty("credit")
    public List<Credit> getCredits() {
        return credits;
    }
    
    @JsonbProperty("credit")
    public void setCredits(final List<Credit> credits) {
        this.credits = credits;
    }
}
