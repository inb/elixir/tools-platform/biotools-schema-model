/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeDeserializer;
import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeListDeserializer;
import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeListSerializer;
import es.bsc.inb.elixir.biotools.model.adapter.EnumTypeSerializer;
import java.util.List;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

/**
 * @author Dmitry Repchevsky
 */

public class Credit {

    private String name;
    private String email;
    private String url;
    private String orcidId;
    private EntityType typeEntity;
    private List<RoleType> typeRoles;
    private String note;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getOrcidId() {
        return orcidId;
    }

    public void setOrcidId(final String orcidId) {
        this.orcidId = orcidId;
    }

    @JsonbTypeDeserializer(EnumTypeDeserializer.class)
    public EntityType getTypeEntity() {
        return typeEntity;
    }

    @JsonbTypeSerializer(EnumTypeSerializer.class)
    public void setTypeEntity(final EntityType typeEntity) {
        this.typeEntity = typeEntity;
    }

    @JsonbTypeDeserializer(EnumTypeListDeserializer.class)
    public List<RoleType> getTypeRoles() {
        return typeRoles;
    }
    
    @JsonbTypeSerializer(EnumTypeListSerializer.class)
    public void setTypeRoles(final List<RoleType> typeRoles) {
        this.typeRoles = typeRoles;
    }

    public String getNote() {
        return note;
    }

    public void setNote(final String note) {
        this.note = note;
    }
}
