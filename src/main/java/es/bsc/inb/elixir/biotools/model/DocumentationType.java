/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

/**
 * @author Dmitry Repchevsky
 */

public enum DocumentationType {
    API_DOCUMENTATION("API documentation"),
    CITATION_INSTRUCTIONS("Citation instructions"),
    CODE_OF_CONDUCT("Code of conduct"),
    COMMANDLINE_OPTIONS("Command-line options"),
    CONTRIBUTIONS_POLICY("Contributions policy"),
    FAQ("FAQ"),
    GENERAL("General"),
    GOVERNANCE("Governance"),
    INSTALLATION_INSTRUCTIONS("Installation instructions"),
    USER_MANUAL("User manual"),
    RELEASE_NOTES("Release notes"),
    TERMS_OF_USE("Terms of use"),
    TRAINING_MATERIAL("Training material"),
    OTHER("Other");
    
    public final String value;
    
    private DocumentationType(final String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static DocumentationType fromValue(final String value) {
        for (DocumentationType type: DocumentationType.values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(value);
    }
}
