/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

import java.util.List;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Function {

    private List<Operation> operations;
    private List<Input> inputs;
    private List<Output> outputs;
    private String note;
    private String cmd;

    @JsonbProperty("operation")
    public List<Operation> getOperations() {
        return operations;
    }

    @JsonbProperty("operation")
    public void setOperations(final List<Operation> operations) {
        this.operations = operations;
    }
    
    @JsonbProperty("input")
    public List<Input> getInputs() {
        return inputs;
    }

    @JsonbProperty("input")
    public void setInputs(final List<Input> inputs) {
        this.inputs = inputs;
    }
    
    @JsonbProperty("output")
    public List<Output> getOutputs() {
        return outputs;
    }

    @JsonbProperty("output")
    public void setOutputs(final List<Output> outputs) {
        this.outputs = outputs;
    }
    
    public String getNote() {
        return note;
    }

    public void setNote(final String note) {
        this.note = note;
    }
    
    public String getCmd() {
        return cmd;
    }

    public void setCmd(final String cmd) {
        this.cmd = cmd;
    }
}
