package es.bsc.inb.elixir.biotools.model.adapter;

import java.util.List;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

/**
 * @author Dmitry Repchevsky
 */

public class EnumTypeListSerializer implements JsonbSerializer<List<Enum>> {

    @Override
    public void serialize(List<Enum> list, JsonGenerator g, SerializationContext ctx) {
        if (list != null) {
            g.writeStartArray();
            for (Enum e : list) {
                g.write(e.toString());
            }
            g.writeEnd();
        }
    }
    
}
