/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model.adapter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import javax.json.JsonArray;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;

/**
 * @author Dmitry Repchevsky
 */

public class EnumTypeListDeserializer implements JsonbDeserializer<List<Enum>> {
    @Override
    public List<Enum> deserialize(JsonParser parser, DeserializationContext ctx, Type type) {
        List<Enum> list = new ArrayList<>();
        
        final ParameterizedType ptype = (ParameterizedType)type;
        final Class<Enum> clazz = (Class)ptype.getActualTypeArguments()[0];
        
        final JsonArray enums = parser.getArray();
        for (int i = 0, n = enums.size(); i < n; i++) {
            final String value = enums.getString(i, null);
            for (Enum e : clazz.getEnumConstants()) {
                if (value.equals(e.toString())) {
                    list.add(e);
                    break;
                }
            }
        }
    
        return list.isEmpty() ? null : list;
    }
}
