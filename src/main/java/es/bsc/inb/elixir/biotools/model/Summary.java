/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

import java.util.List;
import javax.json.bind.annotation.JsonbProperty;

/**
 * @author Dmitry Repchevsky
 */

public class Summary {

    private String name;
    private String description;
    private String homepage;
    private String toolID;
    private String curieID;
    private List<String> versions;
    private List<ExternalId> otherIDs;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(final String homepage) {
        this.homepage = homepage;
    }

    @JsonbProperty("biotoolsID")
    public String getToolID() {
        return toolID;
    }

    @JsonbProperty("biotoolsID")
    public void setToolID(final String toolID) {
        this.toolID = toolID;
    }

    @JsonbProperty("biotoolsCURIE")
    public String getCurieID() {
        return curieID;
    }

    @JsonbProperty("biotoolsCURIE")
    public void setCurieID(final String curieID) {
        this.curieID = curieID;
    }
    
    @JsonbProperty("version")
    public List<String> getVersions() {
        return versions;
    }
    
    @JsonbProperty("version")
    public void setVersions(final List<String> versions) {
        this.versions = versions;
    }
    
    @JsonbProperty("otherID")
    public List<ExternalId> getOtherIDs() {
        return otherIDs;
    }
    
    @JsonbProperty("otherID")
    public void setOtherIDs(final List<ExternalId> otherIDs) {
        this.otherIDs = otherIDs;
    }
}
