/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model.adapter;

import es.bsc.inb.elixir.biotools.model.ToolType;
import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.annotation.JsonbTypeSerializer;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class EnumTypeSerializerTest {
    
    @Test
    public void serialization_test() {
        
        final EnumTypeTestClass tc = new EnumTypeTestClass();
        tc.toolType = ToolType.COMMAND_LINE;
        
        final Jsonb jsonb = JsonbBuilder.create();
        final String json = jsonb.toJson(tc, JsonObject.class);
        final JsonObject obj = jsonb.fromJson(json, JsonObject.class);
        
        Assert.assertEquals(ToolType.COMMAND_LINE.value, obj.getString("toolType"));
    }

    public static class EnumTypeTestClass {
        @JsonbTypeSerializer(EnumTypeSerializer.class)
        public ToolType toolType;
    }
}
