/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.model;

import java.util.Arrays;
import java.util.List;
import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dmitry Repchevsky
 */

public class LabelsTest {
    
    @Test
    public void serialize() {
        
        final Labels labels = new Labels();
        final List<ElixirNode> nodes = Arrays.asList(ElixirNode.SPAIN);
        labels.setElixirNodes(nodes);
        
        final Jsonb jsonb = JsonbBuilder.create();
        final String json = jsonb.toJson(labels, JsonObject.class);
        final JsonObject obj = jsonb.fromJson(json, JsonObject.class);
        Assert.assertEquals(ElixirNode.SPAIN.value, obj.getJsonArray("elixirNode").getString(0));
    }
    
    @Test
    public void deserialize() {
        
        final String json = "{\"elixirNode\":[\"Spain\"]}";
        
        final Jsonb jsonb = JsonbBuilder.create();
        final Labels labels = jsonb.fromJson(json, Labels.class);
        Assert.assertNotNull(labels);
        
        final List<ElixirNode> nodes = labels.getElixirNodes();
        Assert.assertNotNull(nodes);
        Assert.assertEquals(1, nodes.size());
        Assert.assertEquals(ElixirNode.SPAIN, nodes.get(0));
    }
}
