### JSON-B annotated biotools java model classes.

Java [JSON-B](https://javaee.github.io/jsonb-spec/) model for the [biotoolsSchema](https://github.com/bio-tools/biotoolsSchema).
The model reflects [JSON Schema](/schemas/biotools-3.2.0.json) which is inferred from the original XML one.

Java API example:
~~~java
final Jsonb jsonb = JsonbBuilder.create();
final Tool tool = new Tool();
final String json = jsonb.toJson(tool);
~~~

Include the model in maven pom.xml:
~~~java
<dependencies>
    <dependency>
        <groupId>es.bsc.inb.elixir</groupId>
        <artifactId>biotools-schema-model</artifactId>
        <version>3.2.0</version>
    </dependency>
    ...

<repositories>
    <repository>
        <id>inb-bsc-maven</id>
        <url>https://gitlab.bsc.es/inb/maven/raw/master</url>
    </repository>
    ...
~~~